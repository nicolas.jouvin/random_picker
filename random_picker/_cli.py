# pylint: disable=missing-module-docstring


import argparse
import sys

from random_picker._picker import choices


def _positiveint(raw_value):
    value = int(raw_value)
    if value <= 0:
        raise ValueError
    return value


def _create_cli_parser():
    parser = argparse.ArgumentParser(
        prog="random-picker",
        description="Pick randomly one line of a fixed number of line.",
    )

    parser.add_argument(
        "-n",
        "--number-of-lines",
        dest="number",
        help="Number of lines to pick. (Default: 1)",
        type=_positiveint,
        default=1,
    )
    parser.add_argument(
        "-r",
        "--with-replacements",
        help="Randomly pick with replacement (Default: without replacement)",
        dest="replacement",
        action="store_true",
    )
    parser.add_argument(
        "-s",
        "--seed",
        dest="seed",
        help="Seed the pseudo-random-generator with a integer. By default"
        "initialized with unpredictable entropy pulled from the OS.",
        type=int,
        default=None,
    )
    parser.add_argument(
        "input",
        help="Input file. (Default: standard input)",
        nargs="?",
        type=argparse.FileType("r"),
        default=sys.stdin,
    )
    parser.add_argument(
        "output",
        help="Output file. (Default: standard output)",
        nargs="?",
        type=argparse.FileType("w"),
        default=sys.stdout,
    )

    return parser


def main():  # pylint: disable=missing-function-docstring
    parser = _create_cli_parser()
    args = parser.parse_args()
    args.output.write(
        "".join(
            choices(
                values=args.input,
                k=args.number,
                seed=args.seed,
                replacement=args.replacement,
            )
        )
    )
