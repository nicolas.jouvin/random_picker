# pylint: disable=missing-module-docstring


import collections
import heapq
import itertools


import numpy as np


def _infinite_loguniform_gen(shape=(), seed=None):
    rng = np.random.default_rng(seed)
    while True:
        yield from np.log(rng.uniform(size=(256,) + shape))


def _choices_without_replacement(values_weights_iter, k, seed=None):
    value_weights_lpr_iter = zip(
        values_weights_iter, _infinite_loguniform_gen(seed=seed)
    )
    picked = [
        (lpr / w, v) for ((v, w), lpr) in itertools.islice(value_weights_lpr_iter, k)
    ]
    heapq.heapify(picked)

    min_scaled_lpr = picked[0][0]
    for (value, weight), lpr in value_weights_lpr_iter:
        scaled_lpr = lpr / weight
        if scaled_lpr > min_scaled_lpr:
            heapq.heappushpop(picked, (scaled_lpr, value))
            min_scaled_lpr = picked[0][0]

    picked.sort()
    return [value for _, value in picked]


def _choices_with_replacement(values_weights_iter, k, seed=None):
    value_weights_lpr_iter = zip(
        values_weights_iter, _infinite_loguniform_gen(shape=(k,), seed=seed)
    )
    try:
        ((value, weight), lpr) = next(value_weights_lpr_iter)
    except StopIteration:
        return []

    min_scaled_lpr = lpr / weight
    picked = [value] * k

    for (value, weight), lpr in value_weights_lpr_iter:
        scaled_lpr = lpr / weight
        if (scaled_lpr > min_scaled_lpr).any():
            idx = np.where(scaled_lpr > min_scaled_lpr)[0]
            min_scaled_lpr[idx] = scaled_lpr[idx]
            for i in idx:
                picked[i] = value
    return picked


def choices(values, weights=False, k=1, replacement=True, seed=None):
    """Pick randomly N values with or without replacement in the iterable.

    The iterable is consumed by streaming. The iterable must have finite size,
    but the size is not needed. For example, the iterable can be a generator.
    With n the size of the iterable, and k the number of value to pick,
    complexities (for k<<n) are:

    - time complexity: with replacement O(kn), without replacement O(n)
    - memory complexity: O(k)

    If k is not small w.r.t. to the size of the iterable, consider using
    `random` module.

    Parameters
    ----------
    values : Iterable
      Iterable of values containing the values to pick. Must be a iterable of
      pairs (value, weight) when `weights=True`.
    weights : boolean or iterable, optional
      - If weights is False (default), values are picked from uniformly
        distribution over values.
      - If weights is a iterable, values are picked from a distribution with
        probability proportional to weights (weights needs to be positive
        values but do not needs to sum to one).
      - If weights is True, the iterable values is expected to be a iterable of
         two-values tuple containing the value and the weight.
    k : int
      the number of values to pick
    replacement : bool, optional
      indicates if sampling is done with or without replacement. Default: False.
    seed : int, optional
      Seed the pseudo random number generator. If not provided, the generaor is
      seeded with unpredictable entropy pulled from the OS.

    Returns
    -------
    list
      Randomly picked values
    """

    if weights is False:
        values_weights_iter = ((x, 1) for x in values)
    elif isinstance(weights, collections.abc.Iterable):
        values_weights_iter = zip(values, weights)
    elif weights is True:
        values_weights_iter = iter(values)

    if replacement:
        return _choices_with_replacement(values_weights_iter, k, seed=seed)

    return _choices_without_replacement(values_weights_iter, k, seed=seed)


def choice(values, seed=None):
    """Pick randomly a item from a iterable.

    The iterable is consumed by streaming. The iterable must have finite size,
    but the size is not needed. For example, the size can be a generator.
    With n the size of the iterable, complexities are:

    - time complexity: O(n)
    - memory complexity: O(1)

    Parameters
    ----------
    values : Iterable of values
      Iterable of values containing the values to pick.
    seed : int, optional
      Seed the pseudo random number generator. If not provided, the generaor is
      seeded with unpredictable entropy pulled from the OS.

    Raises
    ------
    ValueError
        If something

    Returns
    -------
    Any
      Randomely picked value
    """

    values_lpr_iter = zip(values, _infinite_loguniform_gen(seed=seed))
    try:
        (picked, min_lpr) = next(values_lpr_iter)
    except StopIteration as exc:
        raise ValueError("The iterable must be non-empty") from exc

    for value, lpr in values_lpr_iter:
        if lpr > min_lpr:
            picked = value
            min_lpr = lpr
    return picked
