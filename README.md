# Contributing

Install pre-commit with pip

```bash
pip install pre-commit
```

And set it up

```bash
pre-commit install
```
# Documentation

A static documentation is available at [https://njouvin.pages.mia.inra.fr/random_picker](https://njouvin.pages.mia.inra.fr/random_picker)
